const path = require("path");

module.exports = {
  publicPath: process.env.VUE_APP_SUBFOLDER || "/",
  productionSourceMap: false,
  lintOnSave: true,
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule
      .oneOf("inline")
      .resourceQuery(/inline/)
      .use("vue-svg-loader")
      .loader("vue-svg-loader")
      .end()
      .end()
      .oneOf("external")
      .use("file-loader")
      .loader("file-loader")
      .options({
        name: "assets/[name].[hash:8].[ext]"
      });
    config.resolve.alias.set("vue$", "vue/dist/vue.esm.js");
    config.module.rules.delete("svg");
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.svg$/,
          loader: "vue-svg-loader"
        }
      ]
    }
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/assets/scss/_variables.scss";
              @import "@/assets/scss/_mixins.scss";
              @import "~bootstrap/scss/bootstrap.scss";`
      }
    }
  },
  devServer: {
    disableHostCheck: true
  }
};
