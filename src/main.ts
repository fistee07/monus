import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import VueCookies from "vue-cookies";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
  faEdit,
  faCheck,
  faAngleDoubleDown,
  faUsers,
  faSmoking,
  faGlobe,
  faArrowLeft,
  faArrowRight,
  faBars,
  faMobileAlt,
  faSlash,
  faMapMarkerAlt,
  faMinus,
  faEnvelope,
  faPhone
} from "@fortawesome/free-solid-svg-icons";

import "animate.css";
import "fullpage-vue/src/fullpage.css";
const VueFullpage = require("fullpage-vue");

import vuescroll from "vuescroll";

//I18n imports
import { i18n } from "@/plugins/I18n";

//Vee Validate import
import veeValidateSetup from "@/plugins/VeeValidate";
veeValidateSetup(i18n);

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

library.add(
  faEdit,
  faCheck,
  faAngleDoubleDown,
  faUsers,
  faSmoking,
  faGlobe,
  faArrowLeft,
  faArrowRight,
  faBars,
  faMobileAlt,
  faSlash,
  faMapMarkerAlt,
  faMinus,
  faEnvelope,
  faPhone
);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(VueCookies);
Vue.use(VueFullpage);
Vue.use(vuescroll);

Vue.prototype.$vuescrollConfig = {
  vuescroll: {
    wheelScrollDuration: 350,
    mode: "native",
    sizeStrategy: "percent",
    detectResize: true
  },
  rail: {
    zIndex: 1000
  },
  bar: {
    background: "#e7e0dd"
  }
};

new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount("#app");
