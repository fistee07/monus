import Vue from "vue";
import VeeValidate from "vee-validate";
import { enLanguage } from "@/plugins/localization/en/index.ts";
import { srLanguage } from "@/plugins/localization/sr/index.ts";

const enValidation = enLanguage.global.validation;
const srValidation = srLanguage.global.validation;
const dictionary = {
  en: { messages: enValidation },
  sr: { messages: srValidation }
};

export default function veeValidateSetup(i18n: any) {
  Vue.use(VeeValidate, {
    i18nRootKey: "validations",
    i18n,
    dictionary: dictionary,
    validity: true,
    classes: true,
    classNames: {
      valid: "is-valid"
    }
  });
}
