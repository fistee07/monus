import Vue from "vue";
import Vuei18n from "vue-i18n";

import { enLanguage } from "@/plugins/localization/en/index.ts";
import { srLanguage } from "@/plugins/localization/sr/index.ts";

Vue.use(Vuei18n);

const dateTimeFormats = {
  en: {
    short: {
      year: "numeric",
      month: "short",
      day: "numeric"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      weekday: "short",
      hour: "numeric",
      minute: "numeric",
      hour12: false,
      timeZoneName: "short"
    },
    monthOnly: {
      month: "long"
    }
  },
  sr: {
    short: {
      year: "numeric",
      month: "short",
      day: "numeric"
    },
    long: {
      year: "numeric",
      month: "short",
      day: "numeric",
      weekday: "short",
      hour: "numeric",
      minute: "numeric",
      hour12: false,
      timeZoneName: "short"
    },
    monthOnly: {
      month: "long"
    }
  }
};

const numberFormats = {
  en: {
    currencyAmount: {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }
  },
  sr: {
    currencyAmount: {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }
  }
};

export const i18n = new Vuei18n({
  locale: "en",
  fallbackLocale: "en",
  silentTranslationWarn: true,
  messages: { en: enLanguage, sr: srLanguage },
  dateTimeFormats: dateTimeFormats,
  numberFormats: numberFormats
});
