import { global } from "./global.json";
import { header } from "./header.json";
import { ageVerification } from "./ageVerification.json";
import { home } from "./home.json";
import { footer } from "./footer.json";
import { ourCompany } from "./ourCompany.json";
import { whatWeDo } from "./whatWeDo.json";
import { ourBrands } from "./ourBrands.json";
import { ourBrandsDetails } from "./ourBrandsDetails.json";
import { globalFootprint } from "./globalFootprint.json";
import { news } from "./news.json";
import { contact } from "./contact.json";
import { privacyPolicy } from "./privacyPolicy.json";

export const enLanguage = {
  global,
  header,
  ageVerification,
  home,
  footer,
  ourCompany,
  whatWeDo,
  ourBrands,
  ourBrandsDetails,
  globalFootprint,
  news,
  contact,
  privacyPolicy
};
