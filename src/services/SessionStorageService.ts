export class SessionStorageService {
  public getFromSessionStorage(key: string): string {
    return sessionStorage.getItem(key);
  }

  public parseFromSessionStorage<T>(key: string): T {
    let value = sessionStorage.getItem(key);

    if (!value) return null;

    return JSON.parse(sessionStorage.getItem(key)) as T;
  }

  public removeFromSessionStorage(key: string) {
    sessionStorage.removeItem(key);
  }

  public addToSessionStorage(key: string, obj: any) {
    if (typeof obj === "object")
      sessionStorage.setItem(key, JSON.stringify(obj));
    else sessionStorage.setItem(key, obj);
  }
}
