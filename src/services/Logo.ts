export class LogoService {
  public static getHeaderLogo(): string {
    return require("@/assets/images/monus_logo.svg");
  }

  public static getFooterLogo(): string {
    return require("@/assets/images/monus_logo.svg");
  }
}
