import {
  Cigarrette,
  CigarrettePage,
  CigarretteItem,
  CigarretteNameCategories
} from "@/data-model";

import { LocalizationService } from "./LocalizationService";

import { RouteNameUtil } from "@/utils";

export class CigarretteService {
  private cigarrettes: Cigarrette[] = [];
  private cigarretteNameCategories: CigarretteNameCategories[] = [
    {
      name: "deSantis",
      image: "de-santis-black-octagonal",
      ourBrandsPoster: true,
      categories: [
        {
          name: "classic",
          images: [
            {
              url: "de-santis-black-octagonal",
              name: "deSantisBlack"
            },
            {
              url: "de-santis-red-octagonal",
              name: "deSantisRed"
            },
            {
              url: "de-santis-white-octagonal",
              name: "deSantisWhite"
            }
          ]
        },
        {
          name: "slims",
          images: [
            {
              url: "de-santis-black-slims",
              name: "deSantisBlackSlims"
            },
            {
              url: "de-santis-red-slims",
              name: "deSantisRedSlims"
            },
            {
              url: "de-santis-white-slims",
              name: "deSantisWhiteSlims"
            }
          ]
        }
      ]
    },
    {
      name: "herald",
      image: "herald-black-slims",
      ourBrandsPoster: false,
      categories: [
        {
          name: "slims",
          images: [
            {
              url: "herald-black-slims",
              name: "heraldBlackSlims"
            },
            {
              url: "herald-white-slims",
              name: "heraldWhiteSlims"
            }
          ]
        }
      ]
    },
    {
      name: "happy",
      image: "happy-red-kingsize",
      ourBrandsPoster: false,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "happy-red-kingsize",
              name: "happyRedKingsize"
            },
            {
              url: "happy-blue-kingsize",
              name: "happyBlueKingsize"
            },
            {
              url: "happy-gray-kingsize",
              name: "happyGrayKingsize"
            }
          ]
        }
      ]
    },
    {
      name: "monus",
      image: "monus-red-kingsize",
      ourBrandsPoster: true,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "monus-red-kingsize",
              name: "monusRedKingsize"
            },
            {
              url: "monus-blue-kingsize",
              name: "monusBlueKingsize"
            },
            {
              url: "monus-gray-kingsize",
              name: "monusGrayKingsize"
            }
          ]
        },
        {
          name: "100s",
          images: [
            {
              url: "monus-red-100s",
              name: "monusRed100s"
            },
            {
              url: "monus-blue-100s",
              name: "monusBlue100s"
            },
            {
              url: "monus-gray-100s",
              name: "monusGray100s"
            }
          ]
        },
        {
          name: "slims",
          images: [
            {
              url: "monus-red-slims",
              name: "monusRedSlims"
            },
            {
              url: "monus-blue-slims",
              name: "monusBlueSlims"
            },
            {
              url: "monus-gray-slims",
              name: "monusGraySlims"
            },
            {
              url: "monus-menthol-slims",
              name: "monusMentholSlims"
            },
            {
              url: "monus-grapes-slims",
              name: "monusGrapesSlims"
            },
            {
              url: "monus-apple-slims",
              name: "monusAppleSlims"
            }
          ]
        }
      ]
    },
    {
      name: "nero",
      image: "nero-red-kingsize",
      ourBrandsPoster: true,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "nero-red-kingsize",
              name: "neroRed"
            },
            {
              url: "nero-blue-kingsize",
              name: "neroBlue"
            },
            {
              url: "nero-gold-kingsize",
              name: "neroGoldPlus"
            },
            {
              url: "nero-classic-gold-kingsize",
              name: "neroGold"
            }
          ]
        },
        {
          name: "100s",
          images: [
            {
              url: "nero-red-100s",
              name: "neroRed100s"
            }
          ]
        },
        {
          name: "slims",
          images: [
            {
              url: "nero-red-slims",
              name: "neroRedSlims"
            },
            {
              url: "nero-blue-slims",
              name: "neroBlueSlims"
            }
          ]
        }
      ]
    },
    {
      name: "east",
      image: "east-red-kingsize",
      ourBrandsPoster: false,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "east-red-kingsize",
              name: "eastRedKingsize"
            },
            {
              url: "east-blue-kingsize",
              name: "eastBlueKingsize"
            }
          ]
        },
        {
          name: "100s",
          images: [
            {
              url: "east-red-100s",
              name: "eastRed100s"
            }
          ]
        }
      ]
    },
    {
      name: "floyd",
      image: "floyd-red-kingsize",
      ourBrandsPoster: false,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "floyd-red-kingsize",
              name: "floydRedKingsize"
            },
            {
              url: "floyd-blue-kingsize",
              name: "floydBlueKingsize"
            }
          ]
        }
      ]
    },
    {
      name: "fastRevolution",
      image: "fast-red-kingsize",
      ourBrandsPoster: false,
      categories: [
        {
          name: "kingSize",
          images: [
            {
              url: "fast-red-kingsize",
              name: "fastRedKingsize"
            },
            {
              url: "fast-blue-kingsize",
              name: "fastBlueKingsize"
            }
          ]
        },
        {
          name: "100s",
          images: [
            {
              url: "fast-red-100s",
              name: "fastRed100s"
            },
            {
              url: "fast-blue-100s",
              name: "fastBlue100s"
            }
          ]
        },
        {
          name: "slims",
          images: [
            {
              url: "fast-red-slims",
              name: "fastRedSlims"
            },
            {
              url: "fast-blue-slims",
              name: "fastBlueSlims"
            },
            {
              url: "fast-grapes-slims",
              name: "fastGrapesSlims"
            },
            {
              url: "fast-apple-slims",
              name: "fastAppleSlims"
            }
          ]
        }
      ]
    },
    {
      name: "",
      image: "other-brands",
      ourBrandsPoster: true,
      categories: []
    }
    // {
    //   name: "opposite",
    //   image: "opposite-red",
    //   ourBrandsPoster: false,
    //   categories: [
    //     {
    //       name: "classic",
    //       images: [
    //         {
    //           url: "opposite-red",
    //           name: "oppositeRed"
    //         },
    //         {
    //           url: "opposite-blue",
    //           name: "oppositeBlue"
    //         }
    //       ]
    //     },
    //     {
    //       name: "slims",
    //       images: [
    //         {
    //           url: "opposite-red-slims",
    //           name: "oppositeRedSlims"
    //         },
    //         {
    //           url: "opposite-red-slims",
    //           name: "oppositeBlueSlims"
    //         }
    //       ]
    //     }
    //   ]
    // },
    // {
    //   name: "vronsky",
    //   image: "vronsky-daydream",
    //   ourBrandsPoster: false,
    //   categories: [
    //     {
    //       name: "classic",
    //       images: [
    //         {
    //           url: "vronsky-daydream",
    //           name: "vronskyDaydream"
    //         },
    //         {
    //           url: "vronsky-moonshadow",
    //           name: "vronskyMoonshadow"
    //         }
    //       ]
    //     },
    //     {
    //       name: "slims",
    //       images: [
    //         {
    //           url: "vronsky-daydream-slims",
    //           name: "vronskyDaydreamSlims"
    //         },
    //         {
    //           url: "vronsky-moonshadow-slims",
    //           name: "vronskyMoonshadowSlims"
    //         }
    //       ]
    //     }
    //   ]
    // },
    // {
    //   name: "speed",
    //   image: "speed-blue",
    //   ourBrandsPoster: false,
    //   categories: [
    //     {
    //       name: "classic",
    //       images: [
    //         {
    //           url: "east-red-100s",
    //           name: "eastRed100s"
    //         }
    //       ]
    //     }
    //   ]
    // },
  ];

  public getImage(imageUrl: string): string {
    return require(`@/assets/images/${imageUrl}`);
  }

  private translate(label: string): string {
    return LocalizationService.textTranslate(label);
  }

  private setNewCigarrette(
    id: string,
    image: string,
    name: string,
    ourBrandsPoster: boolean
  ): Cigarrette {
    return new Cigarrette(
      id,
      image,
      this.translate(`ourBrands.cigarretteBlock.${name}.title`),
      this.translate(`ourBrands.cigarretteBlock.${name}.text`),
      ourBrandsPoster,
      new CigarrettePage(
        this.translate(`ourBrandsDetails.${name}.text`),
        this.setNewCategories(name, this.cigarretteNameCategories),
        this.setNewCigarretteItems(name, this.cigarretteNameCategories)
      )
    );
  }

  private setNewCigarrettes(
    cigarrettes: CigarretteNameCategories[]
  ): Cigarrette[] {
    return cigarrettes.map((cigarrette, i): any => {
      return this.setNewCigarrette(
        (++i).toString(),
        `cigarrettes/${cigarrette.image}.png`,
        cigarrette.name,
        cigarrette.ourBrandsPoster
      );
    });
  }

  private setNewCategories(
    name: string,
    cigarrettes: CigarretteNameCategories[]
  ): string[] {
    let categories: string[] = [];
    let cigarrette = cigarrettes.find(cigarrette => cigarrette.name === name);

    cigarrette.categories.forEach(element =>
      categories.push(
        this.translate(`ourBrandsDetails.categories.${element.name}`)
      )
    );

    return categories;
  }

  private setNewCigarretteItems(
    name: string,
    items: CigarretteNameCategories[]
  ): CigarretteItem[] {
    let cigarretteItems: CigarretteItem[] = [];
    let cigarrette = items.find(item => item.name === name);

    cigarrette.categories.forEach(category => {
      category.images.forEach(element => {
        cigarretteItems.push(
          new CigarretteItem(
            this.translate(`ourBrandsDetails.categories.${category.name}`),
            `${element.url}.png`,
            this.translate(`ourBrandsDetails.${name}.${element.name}`)
          )
        );
      });
    });

    return cigarretteItems;
  }

  private apiCigarrette(): Promise<Cigarrette[]> {
    return new Promise(resolve => {
      this.cigarrettes = this.setNewCigarrettes(this.cigarretteNameCategories);

      resolve(this.cigarrettes);
    });
  }

  public getCigarrettes(): Promise<Cigarrette[]> {
    return this.apiCigarrette();
  }

  public getCigarretteData(id: string): Promise<Cigarrette> {
    let self = this;
    return new Promise(resolve => {
      self.cigarrettes.forEach(item => {
        if (RouteNameUtil.getRouteName(item.title) === id) resolve(item);
      });
    });
  }
}
