import Vue from "vue";
import { i18n } from "@/plugins/I18n";
import {
  LANGUAGE_COOKIE_NAME,
  COOKIE_EXPIRE_TIME,
  LANGUAGE_LIST
} from "@/consts/Consts";

export class LocalizationService {
  public static setLanguage(code: string) {
    i18n.locale = code;
  }

  public static languageSwitch(code: string) {
    LocalizationService.setLanguage(code);
    Vue.$cookies.set(LANGUAGE_COOKIE_NAME, code, COOKIE_EXPIRE_TIME);
  }

  public static getLocalization(): string {
    return i18n.locale !== null ? i18n.locale : "en";
  }

  public static getLocalizationFromCookie(): string {
    return Vue.$cookies.get(LANGUAGE_COOKIE_NAME);
  }

  public static getAllLanguages(): string[] {
    return LANGUAGE_LIST;
  }

  public static textTranslate(text: string): string {
    return i18n.t(text) as string;
  }
}
