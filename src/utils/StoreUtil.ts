import { store } from "@/store/index";
import { RESET_STATE } from "@/consts/Consts";

export class StoreUtil {
  public static clearAllStates(): void {
    Object.keys(store.state).forEach(key => {
      store.dispatch(`${key}/${RESET_STATE}`);
    });
  }
}
