export class RouteNameUtil {
  public static getRouteName(routeName: string): string {
    return routeName
      .split(" ")
      .join("-")
      .split(":")
      .join("")
      .toLowerCase();
  }
}
