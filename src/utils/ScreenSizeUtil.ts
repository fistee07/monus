import { isMobile, isMobileOnly, isTablet } from "mobile-device-detect";

export class ScreenSizeUtil {
  // Mobile && Tablet
  public static isMobile() {
    return isMobile;
  }

  public static isMobileOnly() {
    return isMobileOnly;
  }

  public static isTablet() {
    return isTablet;
  }
}
