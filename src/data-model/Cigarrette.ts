import { CigarrettePage } from "./cigarrette/CigarrettePage";

export class Cigarrette {
  id?: string;
  image?: string;
  title?: string;
  shortText?: string;
  ourBrandsPoster?: boolean;
  cigarrettePage?: CigarrettePage;

  constructor(
    id?: string,
    image?: string,
    title?: string,
    shortText?: string,
    ourBrandsPoster?: boolean,
    cigarrettePage?: CigarrettePage
  ) {
    this.id = id;
    this.image = image;
    this.title = title;
    this.shortText = shortText;
    this.ourBrandsPoster = ourBrandsPoster;
    this.cigarrettePage = cigarrettePage;
  }
}
