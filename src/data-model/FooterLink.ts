export class FooterLink {
  name: string;
  to: string;

  constructor(name: string, to: string) {
    this.name = name;
    this.to = to;
  }
}
