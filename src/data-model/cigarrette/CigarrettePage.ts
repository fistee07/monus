import { CigarretteItem } from "./CigarretteItem";

export class CigarrettePage {
  text: string;
  categories: string[];
  items: CigarretteItem[];

  constructor(text: string, categories: string[], items: CigarretteItem[]) {
    this.text = text;
    this.categories = categories;
    this.items = items;
  }
}
