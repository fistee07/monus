import { CigarretteImage } from "./CigarretteImage";

export class CigarretteCategoryImage {
  name: string;
  images: CigarretteImage[];

  constructor(name: string, images: CigarretteImage[]) {
    this.name = name;
    this.images = images;
  }
}
