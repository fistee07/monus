export class CigarretteItem {
  category: string;
  cigarretteImage: string;
  cigarretteName: string;

  constructor(
    category: string,
    cigarretteImage: string,
    cigarretteName: string
  ) {
    this.category = category;
    this.cigarretteImage = cigarretteImage;
    this.cigarretteName = cigarretteName;
  }
}
