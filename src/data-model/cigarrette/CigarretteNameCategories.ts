import { CigarretteCategoryImage } from "./CigarretteCategoryImage";

export class CigarretteNameCategories {
  name: string;
  image: string;
  ourBrandsPoster: boolean;
  categories: CigarretteCategoryImage[];

  constructor(
    name: string,
    image: string,
    ourBrandsPoster: boolean,
    categories: CigarretteCategoryImage[]
  ) {
    this.name = name;
    this.image = image;
    this.ourBrandsPoster = ourBrandsPoster;
    this.categories = categories;
  }
}
