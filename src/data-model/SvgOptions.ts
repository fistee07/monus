export class SvgOptions {
  width?: number;
  height?: number;
  fill?: string;

  constructor(width?: number, height?: number, fill?: string) {
    this.width = width;
    this.height = height;
    this.fill = fill;
  }
}
