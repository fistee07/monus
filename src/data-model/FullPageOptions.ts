export class FullPageOptions {
  start?: number;
  dir?: string;
  duration?: number;
  overflow?: string;
  movingFlag?: boolean;
  currentPage?: number;
  beforeChange?: any;

  constructor(
    start?: number,
    dir?: string,
    duration?: number,
    overflow?: string,
    movingFlag?: boolean,
    beforeChange?: any
  ) {
    this.start = start;
    this.dir = dir;
    this.duration = duration;
    this.overflow = overflow;
    this.movingFlag = movingFlag;
    this.beforeChange = beforeChange;
  }
}
