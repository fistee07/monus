export class SlidePage {
  title?: string;
  text?: any;
  bgImageUrl?: string;
  exploreMoreLink: string;

  constructor(
    title?: string,
    text?: any,
    bgImageUrl?: string,
    exploreMoreLink?: string
  ) {
    this.title = title;
    this.text = text;
    this.bgImageUrl = bgImageUrl;
    this.exploreMoreLink = exploreMoreLink;
  }
}
