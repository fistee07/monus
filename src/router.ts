import Vue from "vue";
import Router, { Route } from "vue-router";
import { i18n } from "@/plugins/I18n";

import { SessionStorageService } from "@/services";

Vue.use(Router);

const AgeVerification = () =>
  import(
    /*webpackChunkName: "ageVerification" */ "./views/AgeVerification.vue"
  );
const HomeOverview = () =>
  import(/*webpackChunkName: "homeOverview" */ "./views/HomeOverview.vue");
const Home = () =>
  import(/*webpackChunkName: "homeOverview" */ "./components/home/Home.vue");
const OurCompany = () =>
  import(/* webpackChunkName: "ourCompany" */ "./views/OurCompany.vue");
const WhatWeDo = () =>
  import(/* webpackChunkName: "whatWeDo" */ "./views/WhatWeDo.vue");
const OurBrands = () =>
  import(/* webpackChunkName: "ourBrands" */ "./views/OurBrands.vue");
const OtherBrands = () =>
  import(/* webpackChunkName: "ourBrands" */ "./views/OtherBrands.vue");
const OurBrandsDetails = () =>
  import(
    /* webpackChunkName: "ourBrandsDetails" */ "./views/OurBrandsDetails.vue"
  );
const OtherBrandsDetails = () =>
  import(
    /* webpackChunkName: "ourBrandsDetails" */ "./views/OtherBrandsDetails.vue"
  );
const GlobalFootprint = () =>
  import(
    /* webpackChunkName: "globalFootprint" */ "./views/GlobalFootprint.vue"
  );
const News = () => import(/* webpackChunkName: "news" */ "./views/News.vue");
const Contact = () =>
  import(/* webpackChunkName: "contact" */ "./views/Contact.vue");
const PageNotFound = () =>
  import(/* webpackChunkName: "pageNotFound" */ "./views/PageNotFound.vue");

// TODO: Update array of cigarrette route names when you add new cigarretteRoute
const cigarretteRouteNames: string[] = [
  "de-santis",
  "monus",
  "nero",
  "herald",
  "happy",
  "east",
  "floyd",
  "fast-revolution"
];

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "*",
      name: "404",
      redirect: "/page-not-found",
      meta: {
        title: "global.websiteTitle"
      }
    },
    {
      path: "/page-not-found",
      name: "pageNotFound",
      component: PageNotFound,
      meta: {
        title: "global.websiteTitle"
      }
    },
    {
      path: "/",
      name: "ageVerification",
      component: AgeVerification,
      meta: {
        title: "global.websiteTitle"
      }
    },
    {
      path: "/homeOverview",
      name: "homeOverview",
      component: HomeOverview,
      redirect: "/home",
      meta: {
        title: "global.websiteTitle"
      },
      beforeEnter: (to, from, next) => {
        if (to.path === from.path) next(false);
        next();
      },
      children: [
        {
          path: "/home",
          name: "home",
          component: Home,
          meta: {
            title: "global.websiteTitle"
          }
        },
        {
          path: "/our-company",
          name: "ourCompany",
          component: OurCompany,
          meta: {
            title: "global.websiteTitle"
          }
        },
        {
          path: "/what-we-do",
          name: "whatWeDo",
          component: WhatWeDo,
          meta: {
            title: "global.websiteTitle"
          }
        },
        {
          path: "/our-brands",
          name: "ourBrands",
          component: OurBrands,
          meta: {
            title: "global.websiteTitle"
          },
          beforeEnter(to, from, next) {
            if (
              (from.name === "ourBrandsDetails" ||
                from.name === "otherBrands ") &&
              to.hash.length < 1
            ) {
              next({ name: "ourBrands", hash: "#cigarrette-block" });
            } else {
              next();
            }
          }
        },
        {
          path: "/our-brands/:name",
          name: "ourBrandsDetails",
          component: OurBrandsDetails,
          props: true,
          meta: {
            title: "global.websiteTitle"
          },
          beforeEnter: (to, from, next) => {
            if (
              cigarretteRouteNames.find(
                routeName => routeName === to.params.name
              )
            )
              next();
            else next("/page-not-found");
          }
        },
        {
          path: "/other-brands",
          name: "otherBrands",
          component: OtherBrands,
          meta: {
            title: "global.websiteTitle"
          },
          beforeEnter(to, from, next) {
            if (from.name === "otherBrandsDetails" && to.hash.length < 1) {
              next({ name: "otherBrands", hash: "#cigarrette-block" });
            } else {
              next();
            }
          }
        },
        {
          path: "/other-brands/:name",
          name: "otherBrandsDetails",
          component: OtherBrandsDetails,
          props: true,
          meta: {
            title: "global.websiteTitle"
          },
          beforeEnter: (to, from, next) => {
            if (
              cigarretteRouteNames.find(
                routeName => routeName === to.params.name
              )
            )
              next();
            else next("/page-not-found");
          }
        },
        {
          path: "/global-footprint",
          name: "globalFootprint",
          component: GlobalFootprint,
          meta: {
            title: "global.websiteTitle"
          }
        },
        {
          path: "/news",
          name: "news",
          component: News,
          meta: {
            title: "global.websiteTitle"
          }
        },
        {
          path: "/contact",
          name: "contact",
          component: Contact,
          meta: {
            title: "global.websiteTitle"
          }
        }
      ]
    }
  ]
});

function setTitle(to: Route) {
  document.title = i18n.t(to.meta.title) as string;
}

function setBodyClass(to: Route) {
  if (to.meta.bodyClass) document.body.className = to.meta.bodyClass;
  else document.body.className = "";
}

router.beforeEach((to, from, next) => {
  setTitle(to);
  setBodyClass(to);

  const publicPages = ["/"];
  const authRequired = !publicPages.includes(to.path);
  const sessionStorageService: SessionStorageService = new SessionStorageService();

  if (
    authRequired &&
    sessionStorageService.parseFromSessionStorage("ageVerified") !== true
  ) {
    return next("/");
  } else if (
    !authRequired &&
    sessionStorageService.parseFromSessionStorage("ageVerified")
  ) {
    if (to.path === "/" && (!from.path || from.path === "/")) {
      next("/home");
    }
    setTitle(from);
    setBodyClass(from);
    return next(false);
  }

  return next();
});

export default router;
