import Vue from "vue";
import Vuex from "vuex";

// import { ipRightModule } from "./modules/ipRightModule";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    // ipRight: ipRightModule,
  }
});
