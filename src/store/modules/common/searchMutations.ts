// import { ChipItem, SelectedCriteria } from "@/data-model";
// import { SearchStoreUtil } from "@/utils";
// import { searchGetters } from "./searchGetters";

// const searchMutations = {
//   updateSelectedCriteria(state: any, selectedCriteria: SelectedCriteria) {
//     let chipId = selectedCriteria.chipId;
//     let value = selectedCriteria.value;
//     let chipProperty = selectedCriteria.chipLocalizedName;

//     let chips = searchGetters.getCriteriaList(state);
//     let chip = searchGetters.getChip(state)(chipId);

//     if (!chip) {
//       SearchStoreUtil.addChipToList(chipId, chips, value, chipProperty);
//     } else {
//       SearchStoreUtil.removeChipFromList(chipId, chips);
//       SearchStoreUtil.addChipToList(chipId, chips, value, chipProperty);
//     }
//     state.CriteriaList = chips;
//     state.CriteriaData[chipId] = value;
//   },

//   clearAll(state: any, nonClearable: string[] = []) {
//     let chips = [...state.CriteriaList] as ChipItem[];
//     chips.forEach(chip => {
//       if (!nonClearable || nonClearable.indexOf(chip.Id) === -1) {
//         SearchStoreUtil.clearValues(chip.Id, state);
//         SearchStoreUtil.removeChipFromList(chip.Id, state.CriteriaList);
//       }
//     });
//   },

//   removeChip: (state: any, chipId: string) => {
//     let chips = state.CriteriaList as ChipItem[];
//     SearchStoreUtil.clearValues(chipId, state);
//     SearchStoreUtil.removeChipFromList(chipId, chips);
//   },

//   clearExceptQuickSearch(state: any) {
//     let chips = state.CriteriaList as ChipItem[];
//     chips
//       .filter(e => e.Id !== "QuickSearch")
//       .map(value => {
//         SearchStoreUtil.clearValues(value.Id, state);
//         SearchStoreUtil.removeChipFromList(value.Id, chips);
//       });
//   },

//   updateFieldsVisible(state: any, visible: boolean) {
//     state.AdditionalFieldsVisible = !visible;
//   }
// };

// export { searchMutations };
