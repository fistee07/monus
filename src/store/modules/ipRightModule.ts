// import { SearchState, SearchCriteria } from "@/data-model/index";
// import { CLEAR_EXCEPT_QUICK_SEARCH } from "@/consts/Consts";

// import { searchGetters } from "./common/searchGetters";
// import { searchActions } from "./common/searchActions";
// import { searchMutations } from "./common/searchMutations";

// function getDefaultState(): SearchState {
//   return {
//     CriteriaList: [],
//     CriteriaData: new SearchCriteria(),
//     AdditionalFieldsVisible: false
//   };
// }

// const state: SearchState = getDefaultState();

// const getters = {
//   ...searchGetters,

//   getAdditionalFieldsVisible: (state: any): boolean => {
//     return state.AdditionalFieldsVisible;
//   }
// };

// const actions = {
//   ...searchActions,

//   clearExceptQuickSearch(context: any) {
//     context.commit(CLEAR_EXCEPT_QUICK_SEARCH);
//   }
// };

// const mutations = {
//   ...searchMutations,

//   resetState(state: any) {
//     Object.assign(state, getDefaultState());
//   }
// };

// export const ipRightModule = {
//   namespaced: true,
//   getters: getters,
//   state: state,
//   actions: actions,
//   mutations: mutations
// };
