export const ENGLISH: string = "en";
export const GERMAN: string = "de";
export const LANGUAGE_COOKIE_NAME: string = "localization";
export const COOKIE_EXPIRE_TIME: string = "6m";
export const LANGUAGE_LIST: string[] = ["en", "de"];
export const INSTRUCTION_SET_EVENT: string = "instriction-set";

export const USER_MANAGEMENT_ROLE: string = "UserManagement";
export const INSTRUCTION_ROLE: string = "Instructions";
export const BUDGET_FORECAST_ROLE: string = "BudgetForecast";
export const PORTFOLIO_MAINTENANCE_ROLE: string = "PortfolioMaintenance";

export const BUDGET_FORECASTS: string = "budget-forecasts";

//Error messages
export const NOT_AUTORIZED: string = "Not Authorized";

//Store modules
export const IP_RIGHT_MODULE: string = "ipRight";
export const BUDGET_FORECAST_MODULE: string = "budgetForecast";
export const PORTFOLIO_MAINTENANCE_MODULE: string = "portfolioMaintenance";

//StoreGetters
export const GET_ADITIONAL_FIELDS_VISIBLE: string =
  "getAdditionalFieldsVisible";
export const GET_CRITERIA_DATA: string = "getCriteriaData";
export const GET_CRITERIA_LIST: string = "getCriteriaList";
export const HAS_CHIPS: string = "hasChips";
export const GET_CHIP: string = "getChip";
export const GET_MODEL_VALUE: string = "getModelValue";
export const GET_SELECTED_PORTFOLIO_FILTER_TYPE: string =
  "getSelectedPortfolioFilterType";

//StoreActions
export const RESET_STATE: string = "resetState";
export const UPDATE_SELECTED_CRITERIA: string = "updateSelectedCriteria";
export const UPDATE_FIELDS_VISIBLE: string = "updateFieldsVisible";
export const CLEAR_ALL: string = "clearAll";
export const REMOVE_CHIP: string = "removeChip";
export const CLEAR_EXCEPT_QUICK_SEARCH: string = "clearExceptQuickSearch";
export const SET_FILTER_TYPE: string = "setFilterType";
export const RESET_DUE_DATE: string = "resetDueDate";
export const ADD_CLIENTS_LIST: string = "addClientsList";
export const SET_ACTIVE_SEARCH: string = "setActiveSearch";
export const UPDATE_CRITERIAS: string = "updateCriterias";

//Dates
export const DATE_EXPORT_FORMAT: string = "YYYY-MM-DD";
export const DEFAULT_MIN_DATE: string = "01/01/1900";
export const DEFAULT_MAX_DATE: string = "01/01/2050";

//Filters
export const ALL_FILTER: string = "ALL";
export const ACTIVE_FILTER: string = "ACTIVE";
export const INACTIVE_FILTER: string = "INACTIVE";
export const BLOCKED_FILTER: string = "BLOCKED";
export const OPEN_FILTER: string = "OPEN";
export const INSTRUCTED_FILTER: string = "INSTRUCTED";
export const EXPORT_FILTER: string = "EXPORT";
export const QUICK_SEARCH_FILTER: string = "quickSearchFilter";
export const BUTTON_FILTER: string = "buttonFilter";

//Statuses
export const ACTIVE_STATUS: string = "Active";
export const ABANDONED_STATUS: string = "Abandoned";
export const INACTIVE_STATUS: string = "Inactive";

export const PREVIOUS_PATH: string = "PreviousPath";

//Font Awesome
export const DOWNLOAD_FONT: string = "download";

//Domains
export const GEVERS_DOMAIN: string = "Gevers";

//Routes
export const PREV_ROUTE: string = "prevRoute";

//Badges
export const REMINDERS_BADGE: string = "remindersBadge";
export const BUDGET_FORECAST_BADGE: string = "budgetForecastBadge";
